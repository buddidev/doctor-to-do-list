import { Component, OnInit } from '@angular/core';
import {TodosModule} from './../../models/todos/todos.module';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos:TodosModule[] | undefined;
  inputTodo:string ="";

  constructor() { }
 
  ngOnInit(): void {

    this.todos = [
      {
        content:'fisrt tot do ',
        completed: false
      },
      {
        content: 'second to do',
        completed:false
      }
    ]
  }

  toggleDone(id:number) {
    this.todos?.map((v,i) => {
      if(i==id) v.completed = !v.completed;
     //console.log(v)
      return v;
    
  })

}

deleteTodo(id:number) {
  this.todos = this.todos?.filter((v,i) => i!=id);
}

addTodo(){

  this.todos?.push({
    content:this.inputTodo,
    completed:false
  });

  this.inputTodo = "";
}
}


